from flask import jsonify, Blueprint, request

bookings = Blueprint('bookings', __name__)


@bookings.route('/')
def list():
    return jsonify(bookings=list())
