import datetime
from flask import Flask, render_template, request, redirect, url_for
import config


def create_app(conf):
    app = Flask(__name__)
    app.config.from_object(conf)

    from bookings import bookings
    app.register_blueprint(bookings, url_prefix='/bookings')

    def get_available_dates_and_resources(date_from):
        return [
            {'date': datetime.datetime.strftime(date_from + datetime.timedelta(days=i), '%Y-%m-%d'),
             'resources': [
                 {'resource_id': 1, 'resource_name': 'Kurt 1'},
                 {'resource_id': 2, 'resource_name': 'Kurt 2'}
             ]} for i in range(7)]

    @app.route('/')
    def index():
        date_from_arg = request.args.get('from')
        date_from = datetime.date.today()
        if date_from_arg:
            try:
                date_from = datetime.datetime.strptime(date_from_arg, '%Y-%m-%d').date()
            except ValueError as e:
                print(date_from_arg, e)
                return redirect(url_for('index'))
        print(date_from_arg, date_from)
        dates_and_resources = get_available_dates_and_resources(date_from)
        return render_template('index.html', dates_and_resources=dates_and_resources)

    return app


app = create_app(config)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
